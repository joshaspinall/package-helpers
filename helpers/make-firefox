#!/bin/bash
#
#    Copyright (C) 2008-2021  Ruben Rodriguez <ruben@trisquel.info>
#    Copyright (C) 2015       Santiago Rodriguez <santi@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

VERSION=81

. ./config

rm debian/control

# Disable remote settings antifeature.
sed '/async fetchLatestChanges.*/areturn;' -i services/settings/Utils.jsm
sed '/async download.*/areturn;' -i services/settings/RemoteSettingsClient.jsm services/settings/Attachments.jsm

# Disable activity-stream antifeatures.
# These are not condensed into a single sed script to make it fail on individual commands that didn't change the source
ActivityStream=browser/components/newtab/lib/ActivityStream.jsm
sed '/^const DEFAULT_SITES/,/^])\;/c const DEFAULT_SITES = new Map\([[""]]\);' -i browser/components/newtab/lib/DefaultSites.jsm
sed '/"showSponsored"/,/value/s/value: true/value: false/' -i $ActivityStream
sed '/"telemetry"/,/value/s/value: true/value: false/' -i $ActivityStream
sed '/"section.highlights.includePocket"/,/value/s/value: true/value: false/' -i $ActivityStream
sed '/"telemetry.structuredIngestion.endpoint"/,/value/s/value: .*/value: ""/' -i $ActivityStream
sed '/read_more_endpoint/,+1s/http.*/",/' -i $ActivityStream
sed '/stories_endpoint/s/getpocket.cdn.mozilla.net/127.0.0.1/' -i $ActivityStream
sed '/stories_referrer/s/http.*/",/' -i $ActivityStream
sed '/topics_endpoint/s/getpocket.cdn.mozilla.net/127.0.0.1/' -i $ActivityStream
sed 's/preffedRegions.includes.geo. .. ..locales .. locales.includes.locale./false/' -i $ActivityStream
sed 's/.Ubuntu., //' -i browser/components/newtab/css/activity-stream*.css
echo "#header-asrouter-container{display: none!important;}" >> browser/components/newtab/css/activity-stream-linux.css

sed '/network.connectivity-service.IPv/s/http.*success.txt?ipv[46]//' -i modules/libpref/init/all.js

# do not alter useragent/platform/oscpu/etc with fingerprinting countermeasure, it makes things worse
sed '/ShouldResistFingerprinting/,/}/s/^/\/\//' -i ./netwerk/protocol/http/nsHttpHandler.cpp
sed '/If fingerprinting resistance is on/,/}/s/^/\/\//' -i ./dom/base/Navigator.cpp

# Replace ubufox recommendation
sed 's/xul-ext-ubufox/xul-ext-youtube-html5-video-player/' -i debian/control.in

# Disable healtreport
cat << EOF > toolkit/components/telemetry/healthreport-prefs.js
pref("datareporting.healthreport.uploadEnabled", false);
pref("datareporting.healthreport.about.reportUrl", "https://trisquel.info/legal");
pref("datareporting.healthreport.infoURL", "https://trisquel.info/legal");
EOF

sed 's%https://www.mozilla.org/legal/privacy/%https://trisquel.info/legal%' -i ./browser/app/profile/firefox.js ./toolkit/content/aboutRights.xhtml

# Remove Google API key
sed '/Google API/,/google-api-keyfile/ d' debian/config/mozconfig.in -i

# Org branding
sed 's/com.ubuntu/org.trisquel/' -i debian/config/mozconfig.in

# Disable DRM support
echo ac_add_options --disable-eme >> debian/config/mozconfig.in
sed '/gmp-clearkey/d' -i ./debian/firefox.install.in

# Locale packages should provide firefox-locale-$LANG
sed "s/Provides.*/Provides: abrowser-locale-@LANGCODE@/" -i debian/control.langpacks

# Provide firefox
sed "s/iceweasel,/iceweasel, firefox,/" -i debian/control.in

# Remove Ubuntu bookmarks
sed -i /ubuntu-bookmarks/d debian/patches/series
rm debian/patches/ubuntu-bookmarks*

# Custom newtab images
sed '/^]$/d' -i browser/components/newtab/data/content/tippytop/top_sites.json
sed 's/}$/},/' -i browser/components/newtab/data/content/tippytop/top_sites.json

cat << EOF >> browser/components/newtab/data/content/tippytop/top_sites.json
  {
    "title": "Trisquel",
    "url": "https://trisquel.info/",
    "image_url": "images/trisquel.png",
    "favicon_url": "favicons/trisquel.ico"
  },
  {
    "title": "GNU",
    "url": "https://www.gnu.org/",
    "image_url": "images/gnu.png",
    "favicon_url": "favicons/gnu.ico"
  },
  {
    "title": "FSF",
    "url": "https://www.fsf.org/",
    "image_url": "images/fsf.png",
    "favicon_url": "favicons/fsf.ico"
  },
  {
    "title": "FSF Directory",
    "url": "https://directory.fsf.org/",
    "image_url": "images/fsf.png",
    "favicon_url": "favicons/fsf.ico"
  },
  {
    "title": "LibrePlanet",
    "url": "https://libreplanet.org/",
    "image_url": "images/libreplanet.png",
    "favicon_url": "favicons/libreplanet.ico"
  },
  {
    "title": "FSF Europe",
    "url": "https://fsfe.org/",
    "image_url": "images/fsfe.png",
    "favicon_url": "favicons/fsfe.ico"
  },
  {
    "title": "Wikipedia",
    "url": "https://www.wikipedia.org/wiki/",
    "image_url": "images/wikipedia.png",
    "favicon_url": "favicons/wikipedia.ico"
  },
  {
    "title": "Wikinews",
    "url": "https://www.wikinews.org/wiki/",
    "image_url": "images/wikinews.png",
    "favicon_url": "favicons/wikinews.ico"
  }
]
EOF

cat << TOP_JSON > services/settings/dumps/main/top-sites.json
{
  "data": [
    {
      "url": "https://trisquel.info/",
      "order": 0,
      "title": "Trisquel",
      "id": "17aeb24c-da88-53bc-8915-c3e3d458070d",
      "last_modified": $(date +%s%N | cut -b1-13)
    },
    {
      "url": "https://www.gnu.org/",
      "order": 1,
      "title": "GNU",
      "id": "2a833806-e5c1-58e9-8b7c-69d9417a1253",
      "last_modified": $(date +%s%N | cut -b1-13)
    },
    {
      "url": "https://www.fsf.org/",
      "order": 2,
      "title": "FSF",
      "id": "d4e3e7e2-64d3-5bac-9c59-3c5c8e1efd8c",
      "last_modified": $(date +%s%N | cut -b1-13)
    },
    {
      "url": "https://directory.fsf.org/",
      "order": 3,
      "title": "FSF Directory",
      "id": "dbff6206-2ada-57ad-8091-8a62ac37be4c",
      "last_modified": $(date +%s%N | cut -b1-13)
    },
    {
      "url": "https://libreplanet.org/",
      "order": 4,
      "title": "LibrePlanet",
      "id": "6f586839-4063-58b8-95ed-62e034e1915e",
      "last_modified": $(date +%s%N | cut -b1-13)
    },
    {
      "url": "https://www.wikipedia.org/",
      "order": 5,
      "title": "Wikipedia",
      "id": "5a18acd7-0440-5512-b3a4-20a35bdc9a1d",
      "last_modified": $(date +%s%N | cut -b1-13)
    }
  ]
}
TOP_JSON

cp $DATA/newtab/*.ico browser/components/newtab/data/content/tippytop/favicons/
cp $DATA/newtab/*.png browser/components/newtab/data/content/tippytop/images/

# Custom DDG
sed '/"params"/,/],/d' -i browser/components/search/extensions/ddg/manifest.json
cp browser/components/search/extensions/ddg browser/components/search/extensions/ddg-html -a
sed 's|/duckduckgo.com/|/html.duckduckgo.com/html/|' -i browser/components/search/extensions/ddg-html/manifest.json
sed 's|DuckDuckGo|DuckDuckGo (HTML)|' -i browser/components/search/extensions/ddg-html/manifest.json
sed 's|ddg@|ddg-html@|' -i browser/components/search/extensions/ddg-html/manifest.json
# disable ads, disable redirect
#sed '/search/s|q=|k1=-1\&kd=-1\&ko=1\&q=|' -i browser/components/search/extensions/ddg/manifest.json

# Reprocess search preconfiguration dump
python3 $DATA/process-json-files.py . browser/components/extensions/schemas/

cat << EOF > debian/distribution.ini
[Global]
id=trisquel
version=$REVISION
about=Abrowser for Trisquel GNU/Linux

[Preferences]
app.distributor = "trisquel"
app.distributor.channel = "trisquel"
app.partner.ubuntu = "trisquel"
intl.locale.requested=""
EOF

sed  "s/^MOZ_APP_NAME\t.*/MOZ_APP_NAME\t\t:= abrowser/;" debian/build/config.mk -i
sed  "s/^MOZ_PKG_NAME\t.*/MOZ_PKG_NAME\t\t:= abrowser/;" debian/build/config.mk -i

############################################################################3
############################################################################3
############################################################################3
sed "s_^Maintainer.*_Maintainer: $DEBFULLNAME <$DEBEMAIL>_g" -i debian/control.in

# Branding files
cp -a $DATA/branding/* browser/branding/official
cp -a $DATA/branding/* browser/branding/unofficial
cp -a $DATA/branding/* browser/branding/nightly
sed '/about-wordmark.svg/d' -i browser/base/content/aboutDialog.css
echo '#warningDesc, #communityExperimentalDesc, #communityDesc, #contributeDesc {display:none!important}' >> browser/base/content/aboutDialog.css
# Disable preprocessor
sed 's/_PP//' -i browser/branding/branding-common.mozbuild
cat << EOF >> debian/config/branch.mk
CHANNEL                 = release
MOZ_WANT_UNIT_TESTS     = 0
MOZ_BUILD_OFFICIAL      = 0
MOZ_ENABLE_BREAKPAD     = 0
MOZ_ENABLE_TELEMETRY    = 0
EOF

SEDSCRIPT="
s|Updates from { -vendor-short-name } and { -brand-product-name }|Updates from { -vendor-short-name }|;
s/Try Firefox/Try Abrowser/g;
s/Firefox Home/Abrowser Home/g;
s/Firefox Account/Sync Account/g;
s/Updates from Mozilla and Firefox/Updates from Mozilla/g;
s/Adobe Flash/Flash/g;
s|www.mozilla.com/firefox/central|trisquel.info/browser|g;
s|mozilla.com/plugincheck|trisquel.info/browser|g;
s|www.mozilla.com/legal/privacy|trisquel.info/legal|g;
s|Firefox Nightly|Abrowser|;
s|Nightly|Abrowser|g;
s|brandProductName=Firefox|brandProductName=Abrowser|;
s|Sign in to Firefox|Sign in to Sync|;
s|Search addons.mozilla.org|Search|g;
s|firefox.settings.services.mozilla.com|127.0.0.1|g;
"

echo "Running batch replace operation"
find . -type f -not -iregex '.*changelog.*' -not -iregex '.*copyright.*' -not -iregex '.*third_party/.*' -execdir /bin/sed --follow-symlinks -i "$SEDSCRIPT" '{}' ';'

echo "Replacing Firefox with Abrowser on ./l10n"
find l10n -type f  -not -iregex '.*changelog.*' -not -iregex '.*copyright.*' -execdir /bin/sed --follow-symlinks -i "s/Firefox/Abrowser/" '{}' ';'
sed 's/Firefox/Abrowser/' -i debian/control.in

sed s/ubuntu/trisquel/g debian/distribution.ini -i
sed "s/ubuntu_version/trisquel_version/; s/Ubuntu 10.10/Trisquel $REVISION/; s/1010/40/" -i debian/firefox.postinst.in

# Delete stuff we don't use and that may contain trademaked logos
rm -rf ./browser/metro ./addon-sdk/source/doc/static-files/media ./browser/themes/windows ./browser/themes/osx ./b2g

#Trisquel custom bookmarks
cp $DATA/bookmarks.html.in browser/locales/generic/profile/bookmarks.html.in

#Trisquel custom search engines
cp -a $DATA/searchplugins/* browser/components/search/extensions/

# install extensions
for extension in $(ls -1 $DATA/extensions/); do
  cp $DATA/extensions/$extension -r debian
  echo "debian/$extension @MOZ_ADDONDIR@/extensions/" >> debian/abrowser.install.in
done

# Disable search field at extensions panel
#sed  '/header-search/d; /search.placeholder/d' -i toolkit/mozapps/extensions/content/extensions.xul
cat << EOF >> toolkit/mozapps/extensions/content/extensions.css
#header-search {
  display:none;
}
EOF

# Hide mobile promo
echo ".fxaMobilePromo {  display: none !important; }" >> browser/themes/shared/preferences/preferences.inc.css
# Hide unwanted elements on about:addons
echo ".discopane-notice, .privacy-policy-link, #main header, #plugindeprecation-notice {  display: none !important; }" >> toolkit/mozapps/extensions/content/aboutaddons.css
# Hide warnings on unsigned extensions
echo ".warning{display: none!important; } .addon[notification=warning]{background-image: none!important;}" >> toolkit/mozapps/extensions/content/extensions.css
# Hide extra links in about box

find -wholename '*/brand.dtd' |xargs /bin/sed 's/trademarkInfo.part1.*/trademarkInfo.part1 "">/' -i

for STRING in rights-intro-point-2 rights-intro-point-3 rights-intro-point-4 rights-intro-point-5 rights-intro-point-6 rights-webservices rights-safebrowsing
do
 find -name aboutRights.ftl | xargs sed -i "s/^$STRING.*/$STRING = /"
done

sed -i 's/<a\ href\=\"http\:\/\/www.mozilla.org\/\">Mozilla\ Project<\/a>/<a\ href\=\"http\:\/\/www.trisquel.info\/\"\>Trisquel\ Project<\/a>/g' browser/base/content/overrides/app-license.html

# We went too far...
#sed -i 's/iceweasel, abrowser, icecat,/iceweasel, firefox, icecat,/g' debian/control.in
sed '/Provides/s/abrowser-locale/firefox-locale/' -i debian/control.langpacks
/bin/sed '/trademarkInfo/s/Abrowser/Firefox/' -i l10n/*/browser/branding/official/brand.ftl

# js settings
cat $DATA/settings.js >> debian/vendor-firefox.js

# set contentblocking to strict by default
patch -p1 < $DATA/default-strict.patch
#sed '/this._trackSlowStartup/s/$/\n\n if (! Services.prefs.prefHasUserValue("browser.contentblocking.category")) { Services.prefs.setStringPref("browser.contentblocking.category", "strict"); this._updateCBCategory; }/' -i ./browser/components/BrowserGlue.jsm

#################
# Android stuff #
#################

# Settings
cat << EOF >> mobile/android/confvars.sh
# Abrowser settings
MOZ_APP_BASENAME=Abrowser
MOZ_APP_VENDOR=Trisquel
MOZ_PAY=0
MOZ_SERVICES_HEALTHREPORT=0
MOZ_SERVICES_HEALTHREPORTER=0
MOZ_SERVICES_METRICS=0
MOZ_DATA_REPORTING=0
MOZ_DEVICES=0
MOZ_ANDROID_GOOGLE_PLAY_SERVICES=0
MOZ_TELEMETRY_REPORTING=0
MOZ_ADDON_SIGNING=0
EOF

# Fix locale
cat << EOF >> mobile/locales/en-US/chrome/region.properties
browser.search.defaultenginename=DuckDuckGo
browser.search.order.1=DuckDuckGo
browser.search.order.2=Google
browser.search.order.3=Yahoo
browser.suggestedsites.restricted_mozilla.url=https://www.trisquel.info
browser.suggestedsites.webmaker.title=GNU Project
browser.suggestedsites.webmaker.url=https://www.gnu.org
EOF

# Branding
rm mobile/android/branding/{official,unofficial,beta,nightly} -rf
cp -a $DATA/android/branding/ mobile/android/branding/official
cp $DATA/android/mobile.ico mobile/android/app/
#cp $DATA/android/images/* mobile/android/themes/core/images/
cp -a $DATA/android/res/main/* mobile/android/geckoview/src/main/res

# Postinst script to manage profile migration and system links
echo '

if [ "$1" = "configure" ] || [ "$1" = "abort-upgrade" ] ; then

[ -f /usr/bin/firefox ] || ln -s /usr/bin/abrowser /usr/bin/firefox

for HOMEDIR in $(grep :/home/ /etc/passwd |grep -v usbmux |grep -v syslog|cut -d : -f 6)
do
    [ -d $HOMEDIR/.mozilla/abrowser ] && continue || true
    [ -d $HOMEDIR/.mozilla/firefox ] || continue
    echo Linking $HOMEDIR/.mozilla/firefox into $HOMEDIR/.mozilla/abrowser
    ln -s $HOMEDIR/.mozilla/firefox $HOMEDIR/.mozilla/abrowser
done 
fi
exit 0 ' >> debian/abrowser.postinst.in

sed 's/1410/65/; s/1310/60/' -i debian/rules

sed 's/^Source:.*/Source: firefox/' -i debian/control.in

debian/rules debian/control
touch -d "yesterday" debian/control
debian/rules debian/control

changelog  "Rebranded for Trisquel"

compile
